import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import reducers from './Reducers';

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

/* eslint-disable no-underscore-dangle */
const configureStore = (onCompletion) => {
  const enhancer = compose(
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  );

  const store = createStore(persistedReducer, enhancer);
  const persistor = persistStore(store, onCompletion);

  return { store, persistor };
};
/* eslint-enable */

export default configureStore;

import React from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// Containers Import
import LandingPage from './Containers/Landing';


// Components Import
import ErrorBoundary from './Components/ErrorBoundary';

// Redux Store Import
import configureStore from './store';

import './styles/index.scss';

// Entry Base App
class BaseApp extends React.Component {
  constructor(props) {
    super(props);

    const configureStoreRef = configureStore(() => this.setState({ isLoading: false }));
    // console.log(configureStoreRef);
    this.state = {
      isLoading: false,
      store: configureStoreRef.store,
      persistor: configureStoreRef.persistor,
    };
  }
  render() {
    /* eslint-disable no-unused-vars */
    const { state: { store, isLoading, persistor } } = this;
    return (
      <ErrorBoundary>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <LandingPage />
          </PersistGate>
        </Provider>
      </ErrorBoundary>
    );
  }
}

export default BaseApp;

import React from 'react';
import PropTypes from 'prop-types';

const InputLabel = ({ text }) => (
  <div className="input-label">{text}</div>
);

InputLabel.propTypes = {
  text: PropTypes.string.isRequired,
};

export default InputLabel;

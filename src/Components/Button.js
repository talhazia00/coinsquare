import React from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const disabledMessage = props.hasSufficientFunds ? 'In-Sufficient Funds' : 'Loading';
  return (
    <button
      className={`button ${props.disabled && 'disabled'}`}
      onClick={props.onClick}
    >
      {props.disabled ? disabledMessage : props.label}
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  hasSufficientFunds: PropTypes.bool,
};

Button.defaultProps = {
  disabled: false,
  hasSufficientFunds: false,
};

export default Button;

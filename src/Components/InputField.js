import React from 'react';
import PropTypes from 'prop-types';

const InputField = ({ value, onValueChange, label, disabled }) => (
  <input
    className="input-field"
    value={value}
    onChange={onValueChange}
    placeholder={label}
    disabled={disabled}
  />
);

InputField.propTypes = {
  value: PropTypes.string.isRequired,
  onValueChange: PropTypes.func.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};

InputField.defaultProps = {
  label: 'Input',
  disabled: false,
};

export default InputField;

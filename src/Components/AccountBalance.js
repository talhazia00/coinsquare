import React from 'react';
import PropTypes from 'prop-types';

const AccountBalance = props => (
  <div className="ab-container">
    <p>Account Balance</p>
    <div className="wallet">
      <div className="usd">
        <p>USD</p>
        <span>{props.usdAmount.toFixed(2)}</span>
      </div>
      <div className="btc">
        <p>BTC</p>
        <span>{props.btcAmount.toFixed(8)}</span>
      </div>
    </div>
  </div>
);

AccountBalance.propTypes = {
  usdAmount: PropTypes.number.isRequired,
  btcAmount: PropTypes.number.isRequired,
};

AccountBalance.defaultProps = {
};

export default AccountBalance;

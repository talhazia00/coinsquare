import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';

import BaseApp from './base';

const wrapper = document.getElementById('main-app');
ReactDOM.render(<BaseApp />, wrapper);

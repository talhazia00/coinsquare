export function setUSDAmount(usdAmount) {
  return {
    type: 'SET_USD',
    usdAmount,
  };
}
export function setBTCAmount(btcAmount) {
  return {
    type: 'SET_BTC',
    btcAmount,
  };
}


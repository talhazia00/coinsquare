export function appHasLoaded() {
  return {
    type: 'CONTENT_LOAD_SUCCESS',
  };
}
export function appHasFailed() {
  return {
    type: 'CONTENT_LOAD_FAILED',
  };
}


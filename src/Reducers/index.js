import { combineReducers } from 'redux';

import AppStateReducer from './AppStateReducer';
import Wallet from './Wallet';

export default combineReducers({
  appState: AppStateReducer,
  wallet: Wallet,
});

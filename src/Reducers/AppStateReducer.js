const initialState = {
  hasContentLoaded: false,
};

export default (state = initialState, action) => {
  if (action.type === 'CONTENT_LOAD_SUCCESS') {
    return {
      ...state,
      hasContentLoaded: true,
    };
  }
  if (action.type === 'CONTENT_LOAD_FAILED') {
    return {
      ...state,
      hasContentLoaded: false,
    };
  }
  return state;
};

const InitialState = {
  usdAmount: 156.12,
  btcAmount: 0,
};

export default (state = InitialState, action) => {
  if (action.type === 'SET_USD') {
    return {
      ...state,
      usdAmount: action.usdAmount,
    };
  }
  if (action.type === 'SET_BTC') {
    return {
      ...state,
      btcAmount: action.btcAmount,
    };
  }
  return state;
};


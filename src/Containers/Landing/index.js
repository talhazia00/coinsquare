import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { appHasLoaded, appHasFailed } from '../../Actions/AppState';

import InputLabel from '../../Components/InputLabel';
import InputField from '../../Components/InputField';
import Button from '../../Components/Button';
import AccountBalance from '../../Components/AccountBalance';
import { setUSDAmount, setBTCAmount } from '../../Actions/Wallet';

class LandingPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fromAmount: '',
      toAmount: '',
      isProcessing: false,
      lastPrice: '',
      fromAmountError: '',
      isConnectToServer: false,
      fetchErrorMessage: '',
    };

    this.handleTradeRequest = this.handleTradeRequest.bind(this);
    this.handleFromAmountChange = this.handleFromAmountChange.bind(this);
    this.getTicker = this.getTicker.bind(this);
  }
  componentDidMount() {
    setTimeout(() => {
      this.getTicker();
    });
  }
  componentDidUpdate(prevProps) {
    const { btcAmount, usdAmount } = this.props;
    if (prevProps.btcAmount !== btcAmount && prevProps.usdAmount !== usdAmount) {
      this.setState({
        fromAmount: '',
        toAmount: '',
      });
    }
  }
  componentWillUnmount() {
    const { isConnectToServer } = this.state;

    if (isConnectToServer) {
      this.socket.close();
    }
  }
  getTicker() {
    this.socket = new WebSocket('wss://api.bitfinex.com/ws/');
    this.socket.addEventListener('open', (ev) => {
      this.socket.send(JSON.stringify({
        event: 'subscribe',
        channel: 'ticker',
        pair: 'BTCUSD',
      }));
      this.setState({
        isConnectToServer: true,
      });
    });

    this.socket.addEventListener('message', (event) => {
      if (typeof event.data.split(',')[7] !== 'undefined') {
        // console.log("price changed", event.data.split(',')[7])
        this.setState({
          lastPrice: event.data.split(',')[7],
        });
      }
    });
  }
  handleTradeRequest() {
    const {
      isProcessing, lastPrice, fromAmount, toAmount,
    } = this.state;
    const {
      usdAmount, btcAmount, setUSDAmount, setBTCAmount,
    } = this.props;
    if (!isProcessing) {
      if (lastPrice !== '') {
        this.setState({
          isProcessing: true,
        });
        const usd_amt = Number(Number(usdAmount) - Number(fromAmount));
        const btc_amt = Number(Number(btcAmount) + Number(toAmount));
        setUSDAmount(usd_amt);
        setBTCAmount(btc_amt);

        setTimeout(() => {
          this.setState({
            isProcessing: false,
          });
        }, 500);
      } else {
        this.setState({
          fetchErrorMessage: 'Sorry, refresh the page and try again.',
        });
      }
    }
  }
  handleFromAmountChange(e) {
    e.preventDefault();
    const { lastPrice } = this.state;
    const fromAmount = isNaN(e.target.value) ? 0 : e.target.value;
    const convertedAmount = (fromAmount / lastPrice).toFixed(8);
    this.setState({
      fromAmount: e.target.value,
      toAmount: isNaN(convertedAmount) ? 0 : convertedAmount,
    });
  }
  //   socket = null;
  render() {
    const { fetchErrorMessage } = this.state;
    const { hasContentLoaded, usdAmount, btcAmount } = this.props;
    const {
      fromAmount, toAmount, isProcessing,
    } = this.state;
    const { handleTradeRequest, handleFromAmountChange } = this;
    // if (!hasContentLoaded) return (<div>Loading</div>);
    return (
      <div className="landing-container">
        <div className="mockup-container">
          <div className="account-balance">
            <AccountBalance usdAmount={usdAmount} btcAmount={btcAmount} />
          </div>
          {fetchErrorMessage && <div>{fetchErrorMessage}</div>}
          <div className="form">
            <InputLabel
              text="Trade"
            />
            <InputField
              value=""
              label="USD"
              onValueChange={() => {}}
              disabled
            />
            {(isNaN(fromAmount)) &&
            <div>Please enter a valid Dollar amount</div>}
            <InputField
              value={fromAmount.toString()}
              onValueChange={e => handleFromAmountChange(e)}
              label="Enter your amount"
            />
            <InputLabel
              text="For"
            />
            <InputField
              value=""
              label="BTC"
              onValueChange={() => {}}
              disabled
            />
            <InputField
              value={toAmount.toString()}
              onValueChange={() => {}}
              label="Display Quote"
            />
            <Button
              label="Trade"
              onClick={handleTradeRequest}
              disabled={isProcessing || (fromAmount > usdAmount)}
              hasSufficientFunds={fromAmount > usdAmount}
            />
          </div>
        </div>
      </div>
    );
  }
}

LandingPage.propTypes = {
  hasContentLoaded: PropTypes.bool,
  updateAppState: PropTypes.func,
  usdAmount: PropTypes.number.isRequired,
  btcAmount: PropTypes.number.isRequired,
};

LandingPage.defaultProps = {
  hasContentLoaded: false,
  updateAppState: () => {},
};

const mapStateToProps = state => ({
  hasContentLoaded: state.appState.hasContentLoaded,
  usdAmount: state.wallet.usdAmount,
  btcAmount: state.wallet.btcAmount,
});

const mapDispatcherToProps = dispatch => ({
  updateAppState: state => (state ? dispatch(appHasLoaded()) : dispatch(appHasFailed())),
  setUSDAmount: amount => dispatch(setUSDAmount(amount)),
  setBTCAmount: amount => dispatch(setBTCAmount(amount)),
});

export default connect(mapStateToProps, mapDispatcherToProps)(LandingPage);

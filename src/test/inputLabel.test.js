import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import InputLabel from '../Components/InputLabel';


describe('InputLabel component test', () => {
  it('should be defined', () => {
    expect(InputLabel).toBeDefined();
  });

  it('show match the snapshot of the field', () => {
    const component = renderer.create(<InputLabel text="text label" />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders its children', () => {
    const text = 'label';
    const renderedComponent = shallow(<InputLabel text={text} />);
    expect(renderedComponent.text()).toBe(text);
  });
});
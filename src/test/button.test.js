import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import Button from '../Components/Button';


describe('Button component test', () => {
  it('should be defined', () => {
    expect(Button).toBeDefined();
  });

  it('show match the snapshot of the button', () => {
    const component = renderer.create(<Button hasSufficientFunds disabled label="Submit" onClick={() => {}} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders its children', () => {
    const text = 'Click me!';
    const renderedComponent = shallow(<Button label={text} />);
    expect(renderedComponent.contains(text)).toEqual(true);
  });

  it('should call mock function when button is clicked', () => {
    const mockFn = jest.fn();
    const component = shallow(<Button hasSufficientFunds disabled label="Submit" onClick={mockFn} />);
    component.simulate('click');
    expect(mockFn).toHaveBeenCalled();
  });
});

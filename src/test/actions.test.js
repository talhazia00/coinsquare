import { appHasFailed, appHasLoaded } from '../Actions/AppState';
import { setUSDAmount, setBTCAmount } from '../Actions/Wallet';


describe('AppState Actions Test', () => {
  it('should return load failed', () => {
    expect(appHasFailed()).toEqual({ type: 'CONTENT_LOAD_FAILED' });
  });

  it('should return load success', () => {
    expect(appHasLoaded()).toEqual({ type: 'CONTENT_LOAD_SUCCESS' });
  });

  it('should create an action with new usd amount', () => {
    const usdAmount = 10;
    const expectedAction = {
      type: 'SET_USD',
      usdAmount,
    };
    expect(setUSDAmount(usdAmount)).toEqual(expectedAction);
  });

  it('should create an action with new btc amount', () => {
    const btcAmount = 0.2543;
    const expectedAction = {
      type: 'SET_BTC',
      btcAmount,
    };
    expect(setBTCAmount(btcAmount)).toEqual(expectedAction);
  });
});
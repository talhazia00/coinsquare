import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import InputField from '../Components/InputField';


describe('InputField component test', () => {
  it('should be defined', () => {
    expect(InputField).toBeDefined();
  });

  it('show match the snapshot of the field', () => {
    const component = renderer.create(<InputField value="field" onValueChange={() => {}} label="placeholder" disabled={false} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('renders its children', () => {
    const value = 'value';
    const mockFn = jest.fn();
    const renderedComponent = shallow(<InputField value={value} label="" disabled={false} onValueChange={mockFn} />);
    expect(renderedComponent.find('input').props().value).toBe(value);
  });

  it('renders its placeholder', () => {
    const placeholder = 'placeholder';
    const mockFn = jest.fn();
    const renderedComponent = shallow(<InputField value="" label={placeholder} disabled={false} onValueChange={mockFn} />);
    expect(renderedComponent.find('input').props().placeholder).toBe(placeholder);
  });

  it('renders disabled', () => {
    const isDisabled = true;
    const mockFn = jest.fn();
    const renderedComponent = shallow(<InputField value="" label="" disabled={isDisabled} onValueChange={mockFn} />);
    expect(renderedComponent.find('input').props().disabled).toBe(isDisabled);
  });

  it('should call mock function when input field is changed', () => {
    const mockFn = jest.fn();
    const component = shallow(<InputField value="value changed" label="" onValueChange={mockFn} />);
    component.find('input').simulate('change', { target: { value: 'matched' } });
    expect(mockFn.mock.calls[0][0].target.value).toBe('matched');
  });
});
import React from 'react';
import { createStore } from 'redux';
import { mount, assert, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import { Server } from 'mock-socket';
import configureMockStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { setUSDAmount, setBTCAmount } from '../Actions/Wallet';
import LandingPage from '../Containers/Landing';
import reducers from '../Reducers';
import Wallet from '../Reducers/Wallet';


describe('Account Balance component', () => {
  const initialState = { hasContentLoaded: true, usdAmount: 90, btcAmount: 0.0454495 };
  const mockStore = configureMockStore(initialState);
  let store,
    wrapper,
    shallowWrapper;

  beforeEach(() => {
    store = createStore(reducers);
    wrapper = mount(<Provider store={store}><LandingPage /></Provider>);
    shallowWrapper = shallow(<Provider store={store}><LandingPage /></Provider>);
  });

  it('should be defined', () => {
    expect(LandingPage).toBeDefined();
  });

//   it('', () => {
//     // expect(wrapper.state('isConnectToServer')).to.equal(true);
//   });

  it('returns initial state for Wallet Reducer', () => {
    expect(Wallet(undefined, {})).toEqual({ usdAmount: 156.12, btcAmount: 0 });
  });

  it('render the LandingPage Container', () => {
    expect(wrapper.find(LandingPage).length).toEqual(1);
  });

  it('render snapshot for LandingPage Container', () => {
    const component = renderer.create(<Provider store={store}><LandingPage /></Provider>);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  // it('basic test', (done) => {
  //   const mockServer = new Server('wss://api.bitfinex.com/ws/');
  //   mockServer.on('connection', (server) => {
  //     mockServer.send(JSON.stringify({
  //       event: 'subscribe',
  //       channel: 'ticker',
  //       pair: 'BTCUSD',
  //     }));
  //   });

  //   // Now when Chat tries to do new WebSocket() it
  //   // will create a MockWebSocket object \
  //   // const Landing = new LandingPage({ store });

  //   setTimeout(() => {
  //     //   const socketLength = Landing.event.data.split(',')[7];
  //     //   assert.equal(socketLength, 7, '7 events where sent from the s server');

  //     mockServer.stop(done);
  //   }, 100);
  // });
});

import React from 'react';
import { mount, shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import AccountBalance from '../Components/AccountBalance';


describe('Account Balance component', () => {
  it('should render snapshot for Account Balance Component', () => {
    const component = renderer.create(<AccountBalance usdAmount={0.5} btcAmount={0.8} />);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
